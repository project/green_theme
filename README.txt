GREEN THEME
---------------

Green theme professional theme with a div based,
fixed width, 3 column layout developed.

The theme is 100% responsive.
INSTALLATION
-------------

Here is the installation procedure for Green Theme

- Theme file can be downloaded from the link
  https://www.drupal.org/project/green_theme
- Extract the downloaded file to the themes directory.
- Goto Admin > Appearance, find Green theme and chose 'Install and
  set as default' option.
- You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in admin area.
Sections include:

- Social Media Link.
- Footer Copyright

1. Enabling Front Page Slider.

For enabling home page slider goto Administrator > Extend, enable
'Home Page Slider'. On enabling 'Home Page Slider'
content type will be created for adding slider data.

2. Social Media Link.

You can add social media links from theme settings which will appear in
the footer section.

3. Team Members listing.

After installing the theme you can enable team members feature from
Administrator > Extend, 'Team'. Then you can add
team members via Content > Add Content > Team members.

4. Portfolio listing.

After installing the theme you can enable team members feature from
Administrator > Extend, 'Portfolio'. Then you can add
portfolio via Content > Add Content > Portfolio.

5. Services.

After installing the theme you can enable services feature from
Administrator > Extend, 'Services'. Then you can add
services via Content > Add Content > Services.

Thank you. Happy Coding...
